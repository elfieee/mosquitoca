# by Yun Li
# last modified on Apr 22, 2016


from PIL import Image
import math
import numpy as np
import matplotlib.pylab as plt # add animation?
import scipy.sparse as sps
plt.rcParams['axes.linewidth'] = 0.01


def reduceimg(imgfile):
    '''converts a map into a numpy array of [R G B Alpha]'s, and 
    then reduces its resolution to 1/10 of the original
    for further processing in the cellular automaton'''
    im = Image.open(imgfile)# size of an A4 = 1275, 1650
    box = (140, 300, 1120, 920) # cropping useful area from map
    im = im.crop(box) # image size = 980, 620
    box_leg = (0, 450, 200, 620) # cropping legend area
    region = Image.new('L', (box_leg[2]-box_leg[0], box_leg[3]-box_leg[1]),\
            color=255)
    im.paste(region, box_leg)
    # im.show()

    # a row array of all pixels
    imdat = np.array(im.getdata())#.reshape((980, 620, 4))

    # the row array converted into a sparse array in gray scale [0,1],
    imsps = np.array([0 if ((1.0 - sum(pt[:3])/(255.0*3) < .1)) else \
            (1.0-sum(pt[:3])/(255.0*3)) for pt in imdat]).reshape((620, 980))
    
    # each row and col of imsps is reduced to 1/10 od its original size
    # elements of imred  \in {0, 1, 2}
    imred = np.zeros((62, 98))
    for ii in range(62):
        for jj in range(98):
            imred[ii][jj] = math.floor(sum([imsps[i][j] \
                    for i in range(ii*10, ii*10+10)\
                    for j in range(jj*10, jj*10+10)]) / 40)
    return imred



def isinmap(pt, mapmat):
    '''finds out whether the pt in consideration is within the mapped area
    given a certain contour'''
    # mapmat = something extracted from the maps
    pass


def onestep(mat):
    '''simulates the population of mosquitoes,
    given an input matrix.'''
    row, col = mat.shape
    temp = np.copy(mat)
    for ii in range(1, row-1):
        for jj in range(1, col-1):
            # to do:
            # check whether this pt is within the map
            temp_val = sum([temp[i][j] for i in range(ii-1,ii+2)\
                    for j in range(jj-1, jj+2)] )
            if temp_val <= 2 or temp_val >= 5:
                mat[ii][jj] = 0 # death
            elif temp_val == 3:
                mat[ii][jj] = 1
            else: # == 4
                mat[ii][jj] = 2
    return mat



def life(sourcefile, step=5):
    '''the main function.
    simulates mosquito life.'''
    mat = reduceimg(sourcefile)
    plotpic(mat, sourcefile[:4] + '_0.png')
    for generation in range(step + 1):
        onestep(mat)
        plotpic(mat, sourcefile[:4] + '_%s.png' % generation)


def plotpic(mat, filename):
    '''saves the plots generated in life().'''
    plt.axis('off')
    dot1 = plt.spy(mat, precision=0, markersize=.5, label='incidence < 2')
    dot2 = plt.spy(mat, precision=1, markersize=3, label='incidence > 2')
    plt.legend([dot1, dot2], ['incidence < 2', 'incidence >2'], loc='best', frameon=False)
    fig = plt.gcf()
    fig.savefig(filename, bbox_inches='tight', pad_inches=0)
    fig.clf()


#life('2007_.png', 2)



